# pe-admin

Toggle the requested execution level of a Portable Executable (PE) files from the command-line.

## Usage
```bash
$ python3 pe-admin.py --help
usage: main.py [-h] --file FILE [--admin ADMIN] [--output OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  The PE file to modify
  --admin ADMIN, -a ADMIN
                        Whether or not to require administrative privileges
                        for the PE file
  --output OUTPUT, -o OUTPUT
                        The output file to write the modified PE to,
                        overwrites source if not specified
```
