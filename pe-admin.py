import argparse
import sys
import re
import tempfile
import os
import pe_tools
import grope
import xml.dom
import xml.dom.minidom

RT_MANIFEST = pe_tools.KnownResourceTypes.RT_MANIFEST
IMAGE_DIRECTORY_ENTRY_RESOURCE = pe_tools.pe_parser.IMAGE_DIRECTORY_ENTRY_RESOURCE

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('boolean value expected')

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', '-f', required=True,
                        help='The PE file to modify')
    parser.add_argument('--admin', '-a', default=False, type=str2bool,
                        help='Whether or not to require administrative privileges for the PE file')
    parser.add_argument('--output', '-o', type=str,
                        help='The output file to write the modified PE to, overwrites source if not specified')
    return parser.parse_args()


def main():
    args = parse_args()

    with open(args.file, 'rb') as ef:
        pe = pe_tools.parse_pe(grope.wrap_io(ef))

        resources = pe.parse_resources()
        
        if RT_MANIFEST in resources:
            for name in resources[RT_MANIFEST]:
                for lang in resources[RT_MANIFEST][name]:
                    man_data = resources[RT_MANIFEST][name][lang]

                    man_data = bytes(man_data).decode('utf-8').rstrip('\x00').encode('utf-8')

                    man_doc = xml.dom.minidom.parseString(bytes(man_data))

                    exec_level = man_doc.getElementsByTagNameNS(
                        "*"
                        "requestedExecutionLevel"
                    )
                    for e in exec_level:
                        e.setAttribute(
                            'level', 'requireAdministrator' if args.admin else 'asInvoker')

                    resources[RT_MANIFEST][name][lang] = man_doc.toxml(
                        encoding='utf-8')

            prepacked = pe_tools.pe_resources_prepack(resources)
            addr = pe.resize_directory(
                IMAGE_DIRECTORY_ENTRY_RESOURCE, prepacked.size)
            pe.set_directory(IMAGE_DIRECTORY_ENTRY_RESOURCE, prepacked.pack(addr))

            if args.output:
                with open(args.output, 'wb') as of:
                    grope.dump(pe.to_blob(), of)
            else:
                of, of_name = tempfile.mkstemp(dir=os.path.split(args.file)[0])
                of = os.fdopen(of, mode='w+b')
                try:
                    grope.dump(pe.to_blob(), of)

                    ef.close()
                    of.close()
                except:
                    of.close()
                    os.remove(of_name)
                    raise
                else:
                    os.remove(args.file)
                    os.rename(of_name, args.file)


if __name__ == '__main__':
    main()
